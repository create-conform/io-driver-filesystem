/////////////////////////////////////////////////////////////////////////////////////////////
//
// io-driver-filesystem
//
//    IO Driver for accessing the host platforms filesystem.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Constants
//
/////////////////////////////////////////////////////////////////////////////////////////////
var PROTOCOL_FILESYSTEM = "file";
var FORMAT_PATH_WINDOWS = "format-path-windows";
var FORMAT_PATH_UNIX =    "format-path-unix";

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var Error =    require("error");
var host =     require("host");
var type =     require("type");
var ioAccess = require("io-access");
var ioURI =    require("io-uri");
var ioStream = require("io-stream");
               require("buffer-to-uint8array");
               require("uint8array-to-string");
               require("string-to-uint8array");
               require("string-to-buffer");

var file;

var nodeFS;
var nodePath;

function requireDependencies() {
    if (!requireNodeDependencies()) {
        throw new Error(host.ERROR_RUNTIME_NOT_SUPPORTED, "The runtime does not support file system access.");
    }
}
function requireNodeDependencies() {
    if (!nodeFS || !nodePath) {
        try {
            nodeFS =   require("fs");
            nodePath = require("path");
        }
        catch(e) {
            return false;
        }
    }
    return true;
}
function createPath(path) {   
    return new Promise(function(resolve, reject) {
        function mkdir(dir) {
            var next;
            var idx = path.indexOf(nodePath.sep, dir.length + 1);
            if (idx >= 0) {
                next = path.substr(0, idx);
            }
            else {
                next = path;
            }

            if (!dir) {
                mkdir(next);
                return;
            }

            if (path == dir) {
                resolve();
                return;
            }

            nodeFS.mkdir(dir, function(err) {
                if (err && err.code != "EEXIST") {
                    reject(new Error("Could not create directory '" + dir + "'. Code: '" + err.code + "'."));
                    return;
                }
                
                // this conditional block would also create the last part in a path (this could be a filename, that's why it is disabled)
                //if (path == dir) {
                //    resolve();
                //}
                //else {
                    mkdir(next);
                //}
            });
        }

        mkdir("");
    });
}

/////////////////////////////////////////////////////////////////////////////////////////////
//
// StreamFileSystem Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function StreamFileSystem(handle, path, access) {
    if (!handle) {
        return null;
    }
    var own = this;

    var written = false;
    var closed = false;

    this.getName = function() {
        if (path && path.lastIndexOf("/") >= 0) {
            return path.substr(path.lastIndexOf("/"));
        }
        else {
            return path;
        }
    };
    this.getLength = function() {
        return new Promise(function(resolve, reject) {
            requireDependencies();

            if (closed) {
                reject(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                return;
            }

            nodeFS.stat(path, function(err,stat) {
                if (err) {
                    if (err.code == "ENOENT") {
                        reject(new Error(ioURI.ERROR_NO_ENTRY, "The file does not exist."));
                    }
                    else if (err.code == "EACCES") {
                        reject(new Error(ioAccess.ERROR_ACCESS_DENIED, ""));
                    }
                    else {
                        reject(err);
                    }
                    return;
                }

                resolve(stat["size"]);
            });
        });
    };
    this.read = function (len, position) {
        if (!position) {
            position = 0;
        }

        return new Promise(function(resolve, reject) {
            requireDependencies();

            if (closed) {
                reject(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                return;
            }

            if (len == null) {
                own.getLength().then(function (length) {
                    doRead(length - position);
                }).catch(reject);
            }
            else {
                doRead(len);
            }

            function doRead(len) {
                var nBuf = new Buffer(len);
                var nPos = position;
                position += len;
                nodeFS.read(handle, nBuf, 0, len, nPos, function(err, bytesRead, buffer) {
                    if (err) {
                        if (err.code == "ENOENT") {
                            reject(new Error(ioURI.ERROR_NO_ENTRY, "The file does not exist."));
                        }
                        else if (err.code == "EACCES") {
                            reject(new Error(ioAccess.ERROR_ACCESS_DENIED, ""));
                        }
                        else {
                            reject(err);
                        }
                        return;
                    }
                    resolve(buffer.toUint8Array());
                });
            }
        });
    };
    this.write = function(data, position) {
        if (!position) {
            position = 0;
        }

        return new Promise(function(resolve, refuse) {
            requireDependencies();

            if (closed) {
                refuse(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                return;
            }

            if (!((typeof Buffer != "undefined" && data instanceof Buffer) || (typeof Uint8Array != "undefined" && data instanceof Uint8Array) || type.isString(data))) {
                refuse(new Error(Error.ERROR_INVALID_PARAMETER, "Invalid parameter 'data'. The parameter should be of type 'Buffer', 'Uint8Array' or 'String'."));
                return;
            }

            var len = data.length;
            var lastError = null;

            var pos = position;
            if (type.isString(data) && pos > 0 && access == ioAccess.OVERWRITE && !written) {
                data = " ".repeat(position) + data;
                pos = 0;
            }

            var streamOptions = { "fd" : handle, "autoClose": false, "start" : pos };
            var writeStream = nodeFS.createWriteStream(null, streamOptions);
            writeStream.on("error", function(err) {
                lastError = err;
            });

            writeStream.write(data instanceof Uint8Array? new Buffer(data) : data, null, function(err) {
                //check if errors occurred since last call, then reset errors
                if (lastError) {
                    if (lastError.code == "ENOENT") {
                        refuse(new Error(ioURI.ERROR_NO_ENTRY, ""));
                    }
                    else if (lastError.code == "EACCES") {
                        refuse(new Error(ioAccess.ERROR_ACCESS_DENIED, ""));
                    }
                    else {
                        refuse(lastError);
                    }
                    return;
                }
                if (err) {
                    if (err.code == "ENOENT") {
                        refuse(new Error(ioURI.ERROR_NO_ENTRY, ""));
                    }
                    else if (err.code == "EACCES") {
                        refuse(new Error(ioAccess.ERROR_ACCESS_DENIED, ""));
                    }
                    else {
                        refuse(err);
                    }
                    return;
                }
                written = true;
                position += len;
                writeStream.end();
                resolve();
            });
        });
    };
    this.close = function (remove) {
        return new Promise(function(resolve, reject) {
            requireDependencies();
            
            if (closed) {
                reject(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                return;
            }

            nodeFS.close(handle, function(err) {
                if (err) {
                    if (err.code == "ENOENT") {
                        reject(new Error(ioURI.ERROR_NO_ENTRY, ""));
                    }
                    else if (err.code == "EACCES") {
                        reject(new Error(ioAccess.ERROR_ACCESS_DENIED, ""));
                    }
                    else {
                        reject(err);
                    }
                    return;
                }
                closed = true;

                if (remove) {
                    nodeFS.unlink(path, function(err) {
                        if (err) {
                            reject(err);
                            return;
                        }
                        resolve();
                    })
                    return;
                }
                resolve();
            });
        });
    };
};
StreamFileSystem.open = function(path, opt_access, opt_create) {
    return new Promise(function(resolve, reject) {
        var access = "";
        var attemptCreate;
        if (!nodeFS) {
            reject(new Error(host.ERROR_RUNTIME, "The runtime does not support access to the local file system."));
            return;
        }
        switch (opt_access) {
            /*  case ioAccess.APPEND:
                    opt_access = "a";
                    break;
                case ioAccess.APPEND_CREATE:
                    opt_access = "a+";
                    break;
                case ioAccess.WRITE:
                    opt_access = "w";
                    break;
                case ioAccess.WRITE_CREATE:
                    opt_access = "w+";
                    break;
                default:
                    opt_access = "r";
                    break;  */
            case ioAccess.MODIFY:
                access = "r+";
                break;
            case ioAccess.OVERWRITE:
                access = "w+";
                break;
            default:
                access = "r";
                break;
        }
        nodeFS.lstat(path, function(err) {
            if (err)
            {
                if (err.code == "ENOENT" && (opt_access == ioAccess.MODIFY || opt_create)) {
                    access = "w+";
                    openFile();
                }
                else {
                    handleError(err);
                }
            }
            else {
                openFile();
            }
        });
        function openFile() {
            nodeFS.open(path, access, function (err, fd) {
                if (err) {
                    handleError(err);
                    return;
                }
                if (!fd) {
                    reject(new Error(host.ERROR_RUNTIME, "The host platform did not return a file handle."));
                    return;
                }
                resolve(new StreamFileSystem(fd, path, opt_access));
            });
        }

        function handleError(err) {
            if (err) {
                if (err.code == "ENOENT" && opt_create && !attemptCreate) {
                    attemptCreate = true;
                    createPath(path).then(function() {
                        openFile();
                    }).catch(function() {
                        reject(new Error(ioURI.ERROR_NO_ENTRY, ""));
                    });
                }
                else if (err.code == "ENOENT") {
                    reject(new Error(ioURI.ERROR_NO_ENTRY, ""));
                }
                else if (err.code == "EACCES") {
                    reject(new Error(ioAccess.ERROR_ACCESS_DENIED, ""));
                }
                else {
                    reject(err);
                }
            }
        }
    });
};
StreamFileSystem.prototype = ioStream;

/////////////////////////////////////////////////////////////////////////////////////////////
//
// URI Handler
//
/////////////////////////////////////////////////////////////////////////////////////////////
var mod = {};
mod.parse = function(uri) {
    /*
        * TYPES
        *   FILESYSTEM
        *      OSX, UNIX, LINUX, ...
        *      /
        *      WINDOWS
        *      [DRIVE LETTER]:\
        *   URL
        *      ./
        *      ../
        *      [PROTOCOL]://[HOSTNAME OR IP]<:[PORT]>/<[PATH]><?[PARAMETERS]>
        *
        */
    if(uri && type.isString(uri)) {
        if ((uri.length >= 1 && uri.substr(0,1) == "/") ||
            (uri.length >= 3 && uri.substr(1,2) == ":\\")) {
            return new ioURI(PROTOCOL_FILESYSTEM, null, uri.replace(/\\/g, "/"), null, null, mod);
        }
        if (uri.length >= 7 && uri.substr(0,7) == PROTOCOL_FILESYSTEM + "://") {
            return new ioURI(uri, mod);
        }
        else if (type.isString(uri)) {// && host.features.includes(host.FEATURE_IO_FILE_SYSTEM)) {
            if (process && process.cwd) {
                var path = (host.platform == host.PLATFORM_WINDOWS? "/" : "") + process.cwd().replace(/\\/g, "/");
                if (path.lastIndexOf("/") != path.length - 1) {
                    path += "/";
                }

                return new ioURI(PROTOCOL_FILESYSTEM, null, uri.length > 0 && uri.substr(0,1) == "/"? uri : path + uri, null, null, mod);
            }
        }
    }
};
mod.open = function(uri, opt_access, opt_create) {
    if (uri && type.isString(uri)) {
        uri = mod.parse(uri);
    }
    else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
        uri = null;
    }
    if (!uri) {
        throw new Error(ioURI.ERROR_INVALID_URI, "");
    }
    return StreamFileSystem.open(uri.toString((process.platform == "win32" ? FORMAT_PATH_WINDOWS : ioURI.FORMAT_PATH), nodePath.sep), opt_access, opt_create);
};
mod.exists = function(uri) {
    return new Promise(function(resolve, reject) {
        requireDependencies();

        if (uri && type.isString(uri)) {
            uri = mod.parse(uri);
        }
        else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
            uri = null;
        }
        if (!uri) {
            reject(new Error(ioURI.ERROR_INVALID_URI, ""));
            return;
        }
        nodeFS.lstat(uri.toString(ioURI.FORMAT_PATH, nodePath.sep), function(err, stats) {
            if (err) {
                if (err.code == "ENOENT") {
                    resolve();
                }
                else if (err.code == "EACCES") {
                    reject(new Error(ioAccess.ERROR_ACCESS_DENIED, ""));
                }
                else {
                    reject(err);
                }
                return;
            }

            if (stats.isDirectory()) {
                resolve(ioURI.ENTRY_DIRECTORY);
            }
            else {
                resolve(ioURI.ENTRY_FILE);
            }
        });
    });

};
mod.delete = function(uri) {
    return new Promise(function(resolve, reject) {
        requireDependencies();

        if (uri && type.isString(uri)) {
            uri = mod.parse(uri);
        }
        else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
            uri = null;
        }
        if (!uri) {
            reject(new Error(ioURI.ERROR_INVALID_URI, ""));
            return;
        }
        nodeFS.unlink(uri.toString(ioURI.FORMAT_PATH, nodePath.sep), function(err) {
            if (err) {
                if (err.code == "ENOENT") {
                    reject(new Error(ioURI.ERROR_NO_ENTRY, ""));
                }
                else if (err.code == "EACCES") {
                    reject(new Error(ioAccess.ERROR_ACCESS_DENIED, ""));
                }
                else {
                    reject(err);
                }
                return;
            }
            resolve();
        });
    });
};
mod.query = function(uri) {
    return new Promise(function(resolve, reject) {
        requireDependencies();
        
        if (uri && type.isString(uri)) {
            uri = mod.parse(uri);
        }
        else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
            uri = null;
        }
        if (!uri) {
            reject(new Error(ioURI.ERROR_INVALID_URI, ""));
            return;
        }
        
        //find volume of uri
        var entries = [];
        var errCount = 0;
        var dir = host.platform == host.PLATFORM_WINDOWS? uri.path.substr(1) : uri.path;
        var lastIdx = dir.lastIndexOf("/");
        if (lastIdx != dir.length - 1) {
            dir = dir.substr(0, lastIdx);
        }
        nodeFS.readdir(dir, function(err, files) {
            if (err) {
                reject(err);
                return;
            }

            if (!files || files.length == 0) {
                resolve([]);
                return;
            }

            files.map(function (file) {
                return nodePath.join(dir, file);
            }).map(function (file) {
                nodeFS.stat(file, function(err, stats) {
                    if (err) {
                        errCount++;
                        return;
                    }

                    try {
                        entries.push(uri.parse((host.platform == host.PLATFORM_WINDOWS? "/" : "") + file + (stats.isFile()? "" : "/")));
                    }
                    catch(e) {
                        console.error(e);
                        errCount++;
                        return;
                    }

                    if (entries.length == (files.length - errCount)) {
                        resolve(entries);
                    }
                });
            });
        });
    });
};
mod.toString = function(uri, opt_format) {
    if (uri && type.isString(uri)) {
        uri = mod.parse(uri);
    }
    else if (uri && (typeof uri.scheme == "undefined" || typeof uri.path == "undefined")) {
        uri = null;
    }
    if (!uri) {
        return "";
    }
    switch (opt_format) {
        case FORMAT_PATH_UNIX:
            return uri.path;
        case FORMAT_PATH_WINDOWS:
            return uri.path.substr(1).replace(/\//g, "\\");
        default:
            return uri.toString();
    }
};
mod.StreamFileSystem = StreamFileSystem;
mod.PROTOCOL_FILESYSTEM = PROTOCOL_FILESYSTEM;
mod.FORMAT_PATH_UNIX = FORMAT_PATH_UNIX;
mod.FORMAT_PATH_WINDOWS = FORMAT_PATH_WINDOWS;

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Register File Protocol Handler
//
/////////////////////////////////////////////////////////////////////////////////////////////
ioURI.protocols.register(mod, PROTOCOL_FILESYSTEM, [ FORMAT_PATH_UNIX, FORMAT_PATH_WINDOWS ]);

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = mod;